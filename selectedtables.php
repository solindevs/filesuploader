<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use local_filesuploader\selected_tables_table;
require_once("../../config.php");
require_once("lib.php");
global $OUTPUT, $PAGE, $CFG, $DB;
require_once($CFG->libdir.'/tablelib.php');
defined('MOODLE_INTERNAL') || die;

$context = context_system::instance();
$PAGE->set_context($context);
require_login();
if (!has_capability('moodle/site:config', $context)) {
    throw new moodle_exception('accessdenied', 'admin');
}
$PAGE->set_pagelayout('admin');
$download = optional_param('download', '', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$query = clean_param(optional_param('q', '', PARAM_TEXT), PARAM_TEXT);

$params = array('page' => $page);
$params['q'] = $query;
$params['section'] = 'filesuploaderselectedtable';
$fileurl = new moodle_url('/local/filesuploader/selectedtables.php', $params);
$baseurl = new moodle_url('/admin/settings.php', $params);

$count = local_filesuploader_update_table_and_get_counts();

$PAGE->set_url($baseurl);
$table = new selected_tables_table('selectedtablestable');
$table->is_downloading($download,
    get_string('selectedtables', 'local_filesuploader').'_'.time(),
    get_string('selectedtables', 'local_filesuploader').'_'.time());

if (!$table->is_downloading()) {
    $PAGE->set_title(get_string('selectedtables', 'local_filesuploader'));
    $PAGE->set_heading(get_string('selectedtables', 'local_filesuploader'));
    echo $OUTPUT->header();
    echo html_writer::tag('h3', get_string('selectedtables', 'local_filesuploader'));
    echo html_writer::tag('h4', get_string('actions', 'local_filesuploader'));
    echo html_writer::tag('hr', '');
    echo html_writer::start_div('row');
    echo html_writer::start_div('col-md-3');
    echo html_writer::tag('h6', get_string('totaltablescount', 'local_filesuploader', $count['countall']));
    echo html_writer::end_div();
    echo html_writer::start_div('col-md-3');
    echo html_writer::tag('h6', get_string('tablescount', 'local_filesuploader', $count['countselected']));
    echo html_writer::end_div();
    echo html_writer::start_div('col-md-3');
    echo html_writer::tag('input', '',
        array('type' => 'submit',
            'name' => 'selectall',
            'id' => 'id_selectall',
            'class' => 'form-submit',
            'value' => get_string('selectallbtn', 'local_filesuploader')));
    echo html_writer::end_div();
    echo html_writer::start_div('col-md-3');
    echo html_writer::tag('input', '',
        array('type' => 'submit',
            'name' => 'unselectall',
            'id' => 'id_unselectall',
            'value' => get_string('unselectallbtn', 'local_filesuploader')));
    echo html_writer::end_div();
    echo html_writer::end_div();
    echo html_writer::tag('hr', '');
    echo html_writer::start_div('form-item clearfix');
    echo html_writer::start_div('form-label');
    echo html_writer::tag('label', 'Search', array('for' => 'id_search'));
    echo html_writer::end_div();
    echo html_writer::start_div('form-setting');
    echo html_writer::tag('input', '',
        array('type' => 'text',
            'name' => 'search',
            'id' => 'id_search',
            'class' => '',
            'value' => isset($query) ? $query : null));
    echo html_writer::end_div();
    echo html_writer::start_div('form-buttons');
    echo html_writer::tag('input', '',
        array('type' => 'submit',
            'name' => 'searchbtn',
            'id' => 'id_searchbtn',
            'class' => 'form-submit',
            'value' => 'Search'));
    echo html_writer::tag('input', '',
        array('type' => 'submit',
            'name' => 'resetsearchbtn',
            'id' => 'id_resetsearchbtn',
            'value' => 'Reset'));
    echo html_writer::end_div();
    echo html_writer::end_div();
}

$columns = array();
$headers = array();
$help = array();
if (isset($query)) {
    $where = "tablename LIKE '%".$DB->sql_like_escape($query)."%'";
} else {
    $where = '1=1';
}
$table->set_sql('*', "{filesuploader_tables}", $where);
if (!$table->is_downloading()) {
    $columns[] = 'selectbox';
    $headers[] = '';
    $help[] = null;
}
$columns[] = 'tablename';
$headers[] = get_string('tablename', 'local_filesuploader');
$help[] = null;
$columns[] = 'selected';
$headers[] = get_string('selectedstatus', 'local_filesuploader');
$help[] = null;
$table->define_columns($columns);
$table->define_headers($headers);
$table->define_help_for_headers($help);
$table->sortable(true, 'tablename');
$table->define_baseurl($fileurl);
$table->out(20, true);

if (!$table->is_downloading()) {
    echo html_writer::tag('br', '');
    echo html_writer::start_div('form-buttons');
    echo html_writer::tag('input', '',
        array('type' => 'submit',
            'name' => 'savepageselection',
            'id' => 'id_savepageselection',
            'class' => 'form-submit',
            'value' => get_string('saveselectionbtn', 'local_filesuploader')));
    echo html_writer::end_div();
    $PAGE->requires->js_call_amd('local_filesuploader/tables_overview', 'init', array());
    echo $OUTPUT->footer();
}

