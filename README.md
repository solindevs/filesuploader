# Plugin /local/filesuploader
SFTP files uploader plugin

# Compatibility
moodle 3.9 or Totara 12

# Author
Solin, Ahmed Landolsi

# Functional description
Used to automatically connect to any SFTP server and upload files from the local Moodle/Totara installation server to this remote server.  
These files are csv exports, of in the plugin settings selected database tables.

# Technical description

## Installation

1. Place the contents of this directory inside the `/<InstallFolder>/local/filesuploader`;
2. Log in into your platform as an administrator and confirm the plugin installation;

## Configuration

### Plugins settings
1. Go to **Site administration > Plugins > Local plugins > Files uploader > Settings**.
2. The settings are split into three categories: SFTP, Files, Export Tables, Retry, Email, Archive and Cleanup. Configure those as you wish and save the changes.

### Scheduled tasks settings
1. Go to **Site administration > Server > Scheduled tasks**.
2. Lookup the **Upload files** (\local_filesuploader\task\upload_files) task and click on the gear icon
2. Configure the appropriate execution interval using the crontab syntax and click to save the changes.

### Manual task execution (verbose trace/debug mode)
1. Authenticate in the server running your platform instance (eg: using SSH);
2. Navigate directories until you are inside the `InstallFolder`;
3. Run the following command: `php admin/tool/task/cli/schedule_task.php --execute='\local_filesuploader\task\upload_files'`

## Execution logs
- Information and error logs can be viewed in the Logs Report. To view them, go to **Site administration > Reports > Logs**.
- You can filter the logs for **Errors** events through the **All activities** dropdown.
