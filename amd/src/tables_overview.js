//This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <http://www.gnu.org/licenses/>.

/**
 * @package local_filesuploader
 * @copyright 2021 Ahmed
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define([ 'jquery', 'core/str', 'core/ajax',
        'core/config', 'core/notification'], function($, str, ajax, mdlcfg, notification) {
    return {
        init : function() {
            var dialoguetitleString, unselectalldialogueString, selectalldialogueString,
            savepageselectiondialogueString, yesbuttonString, nobuttonString, SuccessfullyexecutedString;
            $( "body" ).append( '<div class="modal" id="modal"><div id="floatingBarsG">'+
                    '<div class="blockG" id="rotateG_01"></div>'+
                    '<div class="blockG" id="rotateG_02"></div>'+
                    '<div class="blockG" id="rotateG_03"></div>'+
                    '<div class="blockG" id="rotateG_04"></div>'+
                    '<div class="blockG" id="rotateG_05"></div>'+
                    '<div class="blockG" id="rotateG_06"></div>'+
                    '<div class="blockG" id="rotateG_07"></div>'+
                    '<div class="blockG" id="rotateG_08"></div>'+
                    '</div></div>' );
            str.get_strings([ {
                key : 'dialoguetitle',
                component : 'local_filesuploader'
            }, {
                key : 'unselectalldialogue',
                component : 'local_filesuploader'
            }, {
                key : 'selectalldialogue',
                component : 'local_filesuploader'
            }, {
                key : 'savepageselectiondialogue',
                component : 'local_filesuploader'
            }, {
                key : 'yesbutton',
                component : 'local_filesuploader'
            },{
                key : 'nobutton',
                component : 'local_filesuploader'
            },{
                key : 'Successfullyexecuted',
                component : 'local_filesuploader'
            },]).then(function(str) {
                dialoguetitleString = str[0];
                unselectalldialogueString = str[1];
                selectalldialogueString = str[2];
                savepageselectiondialogueString = str[3];
                yesbuttonString = str[4];
                nobuttonString = str[5];
                SuccessfullyexecutedString = str[6];
            });
            setEventListener();
            function setEventListener() {
                $('#id_searchbtn').on('click', onSearch);
                $('#id_resetsearchbtn').on('click', onResetSearch);
                $('#id_savepageselection').on('click', onSavePageSelection);
                $('#id_unselectall').on('click', onUnselectAll);
                $('#id_selectall').on('click', onSelectAll);
            }
            function get_y(){
                var y = (window.pageYOffset !== undefined) ? window.pageYOffset
                        : (document.documentElement
                        || document.body.parentNode || document.body).scrollTop;
                return y;
            }
            function closemodal(){
                $("#modal").removeClass('visible');
            }
            function openmodal(){
                $("#modal").addClass('visible');
            }
            function removeParam(key, sourceURL) {
                var rtn = sourceURL.split("?")[0],
                    param,
                    params_arr = [],
                    queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
                if (queryString !== "") {
                    params_arr = queryString.split("&");
                    for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                        param = params_arr[i].split("=")[0];
                        if (param === key) {
                            params_arr.splice(i, 1);
                        }
                    }
                    if (params_arr.length) {
                        rtn = rtn + "?" + params_arr.join("&");
                    }
                }
                return rtn;
            }
            function onSearch(event) {
                event.preventDefault();
                var q = $('#id_search').val();
                var url = window.location.href;
                var redirecturl = removeParam('q', url);
                redirecturl = removeParam('page', url);
                window.location.replace(redirecturl+'?page=0&q='+q);
            }
            function onResetSearch(event) {
                event.preventDefault();
                $('#id_search').val('');
                var url = window.location.href;
                var redirecturl = removeParam('q', url);
                window.location.replace(redirecturl);
            }
            function onUnselectAll(event) {
                event.preventDefault();
                notification.confirm(dialoguetitleString, unselectalldialogueString, yesbuttonString, nobuttonString,
                        function() {
                            var data = {sesskey: mdlcfg.sesskey, action : 'unselectall'};
                            ExecuteAction(data);
                        });
            }
            function onSelectAll(event) {
                event.preventDefault();
                notification.confirm(dialoguetitleString, selectalldialogueString, yesbuttonString, nobuttonString,
                        function() {
                            var data = {sesskey: mdlcfg.sesskey, action : 'selectall'};
                            ExecuteAction(data);
                        });
            }
            function onSavePageSelection(event) {
                event.preventDefault();
                var checkrqids = [];
                var uncheckrqids = [];
                $('input:checkbox[id^="selecttable_"]:checked').each(
                        function() {
                            checkrqids.push($(this).attr("name"));
                        });
                $('input:checkbox[id^="selecttable_"]:not(:checked)').each(
                        function() {
                            uncheckrqids.push($(this).attr("name"));
                        });
                notification.confirm(dialoguetitleString, savepageselectiondialogueString, yesbuttonString, nobuttonString,
                        function() {
                            var data = {sesskey : mdlcfg.sesskey, idtocheck : checkrqids, idtouncheck : uncheckrqids, action : 'saveselected'};
                            ExecuteAction(data);
                        });
            }
            function ExecuteAction(data) {
                var theURL = mdlcfg.wwwroot
                        + '/local/filesuploader/actions/actions_handler.php';
                openmodal();
                var y = get_y();
                $.ajax({
                    url : theURL,
                    method : 'POST',
                    dataType : 'json',
                    data : data,
                    success : function(result) {
                        if (result.type == "success") {
                            notification.addNotification({
                                message : SuccessfullyexecutedString,
                                type : "success"
                            });
                            if(y > 100){
                                $("html, body").animate({
                                    scrollTop : 0
                                }, "slow");
                            }
                            setTimeout(function() {
                              location.reload();
                            }, 1000);
                        }else {
                            notification.addNotification({
                                message : JSON.stringify(result),
                                type : "error"
                            });
                            if(y > 100){
                                $("html, body").animate({
                                    scrollTop : 0
                                }, "slow");
                            }
                        }
                    },
                    error : function(response) {
                        notification.addNotification({
                            message : JSON.stringify(response),
                            type : "error"
                        });
                        if(y > 100){
                            $("html, body").animate({
                                scrollTop : 0
                            }, "slow");
                        }
                    },
                    complete : function() {
                        closemodal();
                    }
                });
            }
        }
    };
});