<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once('../../../config.php');
require_once('../lib.php');
global $PAGE;
defined('MOODLE_INTERNAL') || die;

$context = context_system::instance();
$PAGE->set_context($context);
require_login();
require_sesskey();
if (!has_capability('moodle/site:config', $context)) {
    $res = new stdClass();
    $res->type = 'nopermission';
    $res->message = get_string('accessdenied', 'admin');
    echo json_encode($res);
    exit();
}
$idstocheck = optional_param_array('idtocheck', null, PARAM_TEXT);
$idstouncheck = optional_param_array('idtouncheck', null, PARAM_TEXT);
$action = required_param('action', PARAM_TEXT);

try {
    switch ($action){
        case 'saveselected':
            $uncheck = local_filesuploader_unselect_tables_ids($idstouncheck);
            $check = local_filesuploader_select_tables_ids($idstocheck);
            if ($uncheck && $check) {
                $res = new stdClass();
                $res->type = 'success';
                echo json_encode($res);
                exit();
            } else {
                $res = new stdClass();
                $res->type = 'error';
                $res->message = 'Error while saving tables selection.';
                echo json_encode($res);
                exit();
            }
            break;
        case 'selectall':
            if (local_filesuploader_select_all_tables()) {
                $res = new stdClass();
                $res->type = 'success';
                echo json_encode($res);
                exit();
            } else {
                $res = new stdClass();
                $res->type = 'error';
                $res->message = 'Error while unselecting all tables.';
                echo json_encode($res);
                exit();
            }
            break;
        case 'unselectall':
            if (local_filesuploader_unselect_all_tables()) {
                $res = new stdClass();
                $res->type = 'success';
                echo json_encode($res);
                exit();
            } else {
                $res = new stdClass();
                $res->type = 'error';
                $res->message = 'Error while unselecting all tables.';
                echo json_encode($res);
                exit();
            }
            break;
        default:
            exit();
    }
} catch (Exception $exception) {
    $res = new stdClass();
    $res->type = 'error';
    $res->message = $exception;
    echo json_encode($res);
    exit();
}
exit();







