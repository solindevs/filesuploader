<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {

    require_once($CFG->dirroot . '/local/filesuploader/classes/admin_setting_configencryptedpassword.php');

    $ADMIN->add('localplugins', new admin_category(
        'localfilesuploader',
        get_string('pluginname', 'local_filesuploader')
    ));

    $settings = new admin_settingpage('filesuploadersettings', get_string('settings', 'local_filesuploader'));
    $ADMIN->add('localfilesuploader', $settings);
    $settings->add(new admin_setting_heading(
        'sftpsection',
        get_string('sftpsettings', 'local_filesuploader'),
        get_string('sftpsettingsdesc', 'local_filesuploader')
    ));
    $settings->add(new admin_setting_configtext(
        'local_filesuploader/sftphost',
        get_string('sftphost', 'local_filesuploader'),
        get_string('sftphostdesc', 'local_filesuploader'),
        null
    ));
    $settings->add(new admin_setting_configtext(
        'local_filesuploader/sftpport',
        get_string('sftpport', 'local_filesuploader'),
        get_string('sftpportdesc', 'local_filesuploader'),
        22
    ));
    $settings->add(new admin_setting_configselect(
        'local_filesuploader/sftpauthtype',
        get_string('sftpauthtype', 'local_filesuploader'),
        get_string('sftpauthtypedesc', 'local_filesuploader'),
        null,
        array(
            'password' => get_string('password', 'local_filesuploader'),
            'privatekey' => get_string('privatekey', 'local_filesuploader'),
        )
    ));
    $settings->add(new admin_setting_configtext(
        'local_filesuploader/sftpuser',
        get_string('sftpuser', 'local_filesuploader'),
        get_string('sftpuserdesc', 'local_filesuploader'),
        null
    ));
    $settings->add(new \local_filesuploader\admin_setting_configencryptedpassword(
        'local_filesuploader/sftppass',
        get_string('sftppass', 'local_filesuploader'),
        get_string('sftppassdesc', 'local_filesuploader'),
        null
    ));
    $settings->add(new admin_setting_heading(
        'filessection',
        get_string('files', 'local_filesuploader'),
        get_string('filesdesc', 'local_filesuploader')
        ));
    $settings->add(new admin_setting_configtext(
    'local_filesuploader/filespath',
    get_string('filespath', 'local_filesuploader'),
    get_string('filespathdesc', 'local_filesuploader'),
    null
    ));
    $settings->add(new admin_setting_configselect(
        'local_filesuploader/csvencoding',
        get_string('csvencoding', 'local_filesuploader'),
        get_string('csvencodingdesc', 'local_filesuploader'),
        'UTF-8',
        array(
            'ASCII' => 'ASCII',
            'ISO-8859-1' => 'ISO-8859-1',
            'ISO-8859-2' => 'ISO-8859-2',
            'ISO-8859-3' => 'ISO-8859-3',
            'ISO-8859-4' => 'ISO-8859-4',
            'ISO-8859-5' => 'ISO-8859-5',
            'ISO-8859-6' => 'ISO-8859-6',
            'ISO-8859-7' => 'ISO-8859-7',
            'ISO-8859-8' => 'ISO-8859-8',
            'ISO-8859-9' => 'ISO-8859-9',
            'ISO-8859-10' => 'ISO-8859-10',
            'ISO-8859-13' => 'ISO-8859-13',
            'ISO-8859-14' => 'ISO-8859-14',
            'ISO-8859-15' => 'ISO-8859-15',
            'ISO-8859-16' => 'ISO-8859-16',
            'UTF-8' => 'UTF-8',
            'UTF-16' => 'UTF-16',
            'UTF-16BE' => 'UTF-16BE',
            'UTF-16LE' => 'UTF-16LE',
            'UTF-32' => 'UTF-32',
            'UTF-32BE' => 'UTF-32BE',
            'UTF-32LE' => 'UTF-32LE',
            'Windows-1251' => 'Windows-1251',
            'Windows-1252' => 'Windows-1252',
            'Windows-1253' => 'Windows-1253',
            'Windows-1254' => 'Windows-1254',
            'Windows-1255' => 'Windows-1255',
            'Windows-1256' => 'Windows-1256',
            'Windows-1257' => 'Windows-1257',
            'Windows-1258' => 'Windows-1258',
        )
        ));
    $tablesurl = new moodle_url('/local/filesuploader/selectedtables.php');
    $link = html_writer::tag('a',
        get_string('linktotables', 'local_filesuploader'),
        array(
            'href' => $tablesurl,
            'target' => '_blank'
        ));
    $settings->add(new admin_setting_heading(
    'exporttablessection',
    get_string('exporttablessection', 'local_filesuploader'),
    get_string('exporttablessectiondesc', 'local_filesuploader').$link
    ));
    $settings->add(new admin_setting_configselect(
        'local_filesuploader/csvseparator',
        get_string('csvseparator', 'local_filesuploader'),
        get_string('csvseparatordesc', 'local_filesuploader'),
        null,
        array(
            ',' => 'comma ","',
            ':' => 'colon ":"',
            ';' => 'semicolon ";"',
            '\t' => 'tab',
            '|' => '|',
            'custom' => 'custom (must be defined)',
        )
        ));
    $settings->add(new admin_setting_configtext(
        'local_filesuploader/customseparator',
        get_string('customseparator', 'local_filesuploader'),
        get_string('customseparatordesc', 'local_filesuploader'),
        null
        ));
    $settings->add(new admin_setting_heading(
        'retrysection',
        get_string('retrysection', 'local_filesuploader'),
        get_string('retrysectiondesc', 'local_filesuploader')
        ));
    $settings->add(new admin_setting_configcheckbox(
        'local_filesuploader/retryenabled',
        get_string('retryenabled', 'local_filesuploader'),
        get_string('retryenableddesc', 'local_filesuploader'),
        null
    ));
    $settings->add(new admin_setting_configselect(
        'local_filesuploader/retrytime',
        get_string('retrytime', 'local_filesuploader'),
        get_string('retrytimedesc', 'local_filesuploader'),
        null,
        array(
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
        )
        ));
    $settings->add(new admin_setting_configduration(
        'local_filesuploader/retryinterval',
        get_string('retryinterval', 'local_filesuploader'),
        get_string('retryintervaldesc', 'local_filesuploader'),
        10 * MINSECS
        ));
    $settings->add(new admin_setting_heading(
        'emailsection',
        get_string('emailsection', 'local_filesuploader'),
        get_string('emailsectiondesc', 'local_filesuploader')
        ));
    $settings->add(new admin_setting_configcheckbox(
        'local_filesuploader/emailenabled',
        get_string('emailenabled', 'local_filesuploader'),
        get_string('emailenableddesc', 'local_filesuploader'),
        null
        ));
    $settings->add(new admin_setting_configcheckbox(
        'local_filesuploader/emaildebugenabled',
        get_string('emaildebugenabled', 'local_filesuploader'),
        get_string('emaildebugenableddesc', 'local_filesuploader'),
        null
        ));
    $settings->add(new admin_setting_configtextarea(
        'local_filesuploader/emailaddress',
        get_string('emailaddress', 'local_filesuploader'),
        get_string('emailaddressdesc', 'local_filesuploader'),
        null
        ));
    $settings->add(new admin_setting_heading(
        'archivesection',
        get_string('archivesection', 'local_filesuploader'),
        get_string('archivesectiondesc', 'local_filesuploader')
        ));
    $settings->add(new admin_setting_configcheckbox(
        'local_filesuploader/archiveenabled',
        get_string('archiveenabled', 'local_filesuploader'),
        get_string('archiveenableddesc', 'local_filesuploader'),
        null
    ));
    $settings->add(new admin_setting_configtext(
        'local_filesuploader/archivepath',
        get_string('archivepath', 'local_filesuploader'),
        get_string('archivepathdesc', 'local_filesuploader'),
        null
    ));
    $settings->add(new admin_setting_heading(
        'cleanupsection',
        get_string('cleanupsection', 'local_filesuploader'),
        get_string('cleanupsectiondesc', 'local_filesuploader')
        ));
    $settings->add(new admin_setting_configcheckbox(
        'local_filesuploader/cleanupenabled',
        get_string('cleanupenabled', 'local_filesuploader'),
        get_string('cleanupenableddesc', 'local_filesuploader'),
        null
    ));
    $settings->add(new admin_setting_configduration(
        'local_filesuploader/cleanupolderthan',
        get_string('cleanupolderthan', 'local_filesuploader'),
        get_string('cleanupolderthandesc', 'local_filesuploader'),
        100 * DAYSECS
    ));
    $seletedtablespage = new admin_externalpage('filesuploaderselectedtable',
        get_string('selectedtables', 'local_filesuploader'),
        new moodle_url("$CFG->wwwroot/local/filesuploader/selectedtables.php"),
        'moodle/site:config');
    $ADMIN->add('localfilesuploader', $seletedtablespage);
}
