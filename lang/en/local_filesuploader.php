<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Files uploader';
$string['archivepath'] = 'Archive path';
$string['archivepathdesc'] = 'The path in the SFTP server where to archive processed files.<br>The original file names are preserved after archival.<br>Absolute path example: /absolute/path/to/archive<br>Relative path example: relative/path/to/archive';
$string['uploadfiles'] = 'Upload files';
$string['emailaddress'] = 'Email addresses';
$string['emailaddressdesc'] = 'List of email addresses (comma separated)';
$string['errormailsubject'] = '{$a} - SFTP files upload error';
$string['sftpsettings'] = 'SFTP settings';
$string['sftpsettingsdesc'] = '';
$string['sftpauthtype'] = 'Authentication type';
$string['sftpauthtypedesc'] = '';
$string['sftphost'] = 'Host';
$string['sftphostdesc'] = 'Either use the IP (e.g.: 192.168.0.1) or the host name (e.g.: ftp-server.com) of the SFTP server.';
$string['sftpport'] = 'Port';
$string['sftpportdesc'] = 'The port used by the SFTP server to accept connections.';
$string['sftpuser'] = 'User';
$string['sftpuserdesc'] = 'The username used to authenticate with the SFTP server.';
$string['sftppass'] = 'Password or Private Key';
$string['sftppassdesc'] = 'Either the password or the private key of the username specified above.';
$string['filespath'] = 'Files path';
$string['filespathdesc'] = 'The path in the SFTP server where to save processed files';
$string['files'] = 'Files';
$string['filesdesc'] = '';
$string['settings'] = 'Settings';
$string['password'] = 'Password';
$string['privatekey'] = 'Private key';
$string['retryinterval'] = 'Retry interval';
$string['retryintervaldesc'] = '';
$string['delete'] = 'Delete';
$string['csvencoding'] = 'CSV encoding';
$string['csvencodingdesc'] = 'Specify the encoding used in the CSV files. If the wrong encoding is used, data corruption may occur.';
$string['selectedtables'] = 'Select database tables';
$string['exporttablessection'] = 'Export Tables';
$string['exporttablessectiondesc'] = '';
$string['csvseparator'] = 'CSV separator';
$string['csvseparatordesc'] = 'Specify the CSV separator used in the CSV files.';
$string['retrysection'] = 'Retry';
$string['retrysectiondesc'] = '';
$string['retryenabled'] = 'Enable retry';
$string['retryenableddesc'] = '';
$string['retrytime'] = 'Retry time';
$string['retrytimedesc'] = '';
$string['emailsection'] = 'Email';
$string['emailsectiondesc'] = '';
$string['emailenabled'] = 'Enable emails';
$string['emailenableddesc'] = 'User will be notifyed if upload task failed.';
$string['emaildebugenabled'] = 'Enable debug emails';
$string['emaildebugenableddesc'] = 'User will be notifyed if upload task executed.';
$string['archivesection'] = 'Archive';
$string['archivesectiondesc'] = '';
$string['archiveenabled'] = 'Enable archive';
$string['archiveenableddesc'] = '';
$string['cleanupsection'] = 'Cleanup';
$string['cleanupsectiondesc'] = '';
$string['cleanupenabled'] = 'Enable cleanup';
$string['cleanupenableddesc'] = 'If archive disabled this will delete old files before upload else if archive is enabled then please specify a time interval (older than).';
$string['cleanupolderthan'] = 'Cleanup files older than';
$string['cleanupolderthandesc'] = '';
$string['filenameconvention'] = 'File naming convention';
$string['filenameconventiondesc'] = '';
$string['customseparator'] = 'Custom separator';
$string['customseparatordesc'] = 'Specify custom separatom if "custom" is the selected separator';
$string['tablename'] = 'Table name';
$string['selectedstatus'] = 'Selected status';
$string['showalltables'] = 'Show all tables';
$string['selectedtables'] = 'Select tables for export';
$string['selectallbtn'] = 'Select all tables';
$string['saveselectionbtn'] = 'Save page selection';
$string['unselectallbtn'] = 'Unselect all tables';
$string['dialoguetitle'] = 'Confirm action:';
$string['unselectalldialogue'] = 'Are you sure to unselect all selected database tables for export?';
$string['selectalldialogue'] = 'Are you sure to select all database tables for export?';
$string['savepageselectiondialogue'] = 'Are you sure to save on this page selected database tables for export?';
$string['Successfullyexecuted'] = 'Changes were successfully saved.';
$string['yesbutton'] = 'Yes';
$string['nobutton'] = 'No';
$string['actions'] = 'Either select / unselect all tables or each page can be modified and saved separately.';
$string['tablescount'] = '<b>{$a}</b> tables are selected.';
$string['select_table_eventname'] = 'Database table selected';
$string['unselect_table_eventname'] = 'Database table unselected';
$string['select_all_tables_eventname'] = 'All database tables selected';
$string['unselect_all_tables_eventname'] = 'All database tables unselected';
$string['totaltablescount'] = 'Total table number: <b>{$a}</b>.';
$string['linktotables'] = 'Select database tables which should be exported';
$string['successmailsubject'] = '{$a} - SFTP files uploaded successfuly';





