<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
use local_filesuploader\event\all_tables_selected;
use local_filesuploader\event\all_tables_unselected;
use local_filesuploader\event\table_selected;
use local_filesuploader\event\table_unselected;

function local_filesuploader_update_table_and_get_counts() {
    global $DB;
    $actualtables = array();
    $actualtables = $DB->get_tables(true);
    // Make sure we have always all the DB tables names stored in the table.
    foreach ($actualtables as $table) {
        if (!$DB->record_exists('filesuploader_tables', array('tablename' => $table))) {
            $DB->insert_record('filesuploader_tables', array('tablename' => $table, 'selected' => 0));
        }
    }
    // Make sure we dont have any old tables (from deinstalled plugins).
    $tables = $DB->get_records('filesuploader_tables', array(), '', 'tablename');
    $oldtables = array_filter($tables, function($k) use ($actualtables) {
        return ! in_array($k, $actualtables);
    }, ARRAY_FILTER_USE_KEY);
    $DB->delete_records_list('filesuploader_tables', 'tablename', array_keys($oldtables));

    $countselected = $DB->count_records('filesuploader_tables', array('selected' => 1));
    $countall = $DB->count_records('filesuploader_tables');
    return ['countselected' => $countselected, 'countall' => $countall];
}

function local_filesuploader_select_all_tables() {
    global $DB, $USER;
    if ($DB->set_field('filesuploader_tables', 'selected', 1)) {
        $eventparam = array(
            'other' => array(
                'userid' => $USER->id
            )
        );
        $event = all_tables_selected::create($eventparam);
        $event->trigger();
        return true;
    } else {
        return false;
    }
}

function local_filesuploader_unselect_all_tables() {
    global $DB, $USER;
    if ($DB->set_field('filesuploader_tables', 'selected', 0)) {
        $eventparam = array(
            'other' => array(
                'userid' => $USER->id
            )
        );
        $event = all_tables_unselected::create($eventparam);
        $event->trigger();
        return true;
    } else {
        return false;
    }
}

function local_filesuploader_select_tables_ids($ids) {
    global $DB, $USER;
    foreach ($ids as $id) {
        if (1 != $DB->get_field('filesuploader_tables', 'selected', array('id' => $id))) {
            $DB->set_field('filesuploader_tables', 'selected', 1, array('id' => $id));
            $tname = $DB->get_field('filesuploader_tables', 'tablename', array('id' => $id));
            $eventparam = array(
                'other' => array(
                    'tname' => $tname,
                    'userid' => $USER->id
                )
            );
            $event = table_selected::create($eventparam);
            $event->trigger();
        }
    }
    return true;
}

function local_filesuploader_unselect_tables_ids($ids) {
    global $DB, $USER;
    foreach ($ids as $id) {
        if (0 != $DB->get_field('filesuploader_tables', 'selected', array('id' => $id))) {
            $DB->set_field('filesuploader_tables', 'selected', 0, array('id' => $id));
            $tname = $DB->get_field('filesuploader_tables', 'tablename', array('id' => $id));
            $eventparam = array(
                'other' => array(
                    'tname' => $tname,
                    'userid' => $USER->id
                )
            );
            $event = table_unselected::create($eventparam);
            $event->trigger();
        }
    }
    return true;
}










