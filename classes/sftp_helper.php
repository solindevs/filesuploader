<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_filesuploader;

defined('MOODLE_INTERNAL') || die();

use local_filesuploader\event\sftp_error;
use phpseclib3\Crypt\RSA;
use phpseclib3\Net\SFTP;

class sftp_helper
{
    public static function iterate_archives($archivepath) {
        $sftp = static::get_sftp_client();
        $config = config::get();
        if (!$sftp) {
            return false;
        }
        $dirs = $sftp->nlist($archivepath, false);
        if (empty($dirs)) {
            $dirs = array();
        }
        foreach ($dirs as $dir) {
            if ((!$sftp->is_dir($dir)) && ($dir !== '.') && ($dir !== '..')) {
                $oldthan = $config->cleanupolderthan;
                $date = date("Ymd-Hi", time() - date("Z"));
                $now = strtotime($date);
                $dirtimestamp = strtotime($dir);
                if ($now - $dirtimestamp > $oldthan) {
                    $filepath = "{$archivepath}/{$dir}";
                    $sftp->delete($filepath);
                }
            }
        }
    }

    public static function iterate_files($globalpath, $archivepath) {
        $sftp = static::get_sftp_client();
        $config = config::get();
        if (!$sftp) {
            return false;
        }
        $sftp->setListOrder('filename', SORT_DESC);
        $files = $sftp->nlist($globalpath, false);
        if (empty($files)) {
            $files = array();
        }
        $count = 0;
        foreach ($files as $file) {
            if (!$sftp->is_dir($file)) {
                $count++;
                $filepath = "{$globalpath}/{$file}";
                if ($archivepath) {
                    // If archive enabled get first file name and cretae folder in archive path.
                    // With the timestamp from first filename.
                    if ($count == 1) {
                        $re = '/\d{8}-\d{4}/m';
                        $matches = array();
                        preg_match_all($re, $file, $matches, PREG_SET_ORDER, 0);
                        $foldername = $matches[0][0];
                        $dir = "{$archivepath}/{$foldername}";
                        if ($foldername) {
                            $sftp->mkdir($dir);
                        }
                    }
                    $archive = "{$dir}/{$file}";
                    $sftp->rename($filepath, $archive);
                }
                if ($config->cleanupenabled == 1) {
                    // If cleanup enabled delete last created files from upload path.
                    $sftp->delete($filepath);
                }
            }
        }
        if ($archivepath) {
            // If archive enabled and cleanup enabled delete archive older than x time.
            if ($config->cleanupenabled == 1) {
                static::iterate_archives($archivepath);
            }
        }
        $sftp->disconnect();
        return true;
    }

    /**
     * Checks whether a path exists, is directory and is wirteable
     *
     * @param string $path
     * @return bool
     * @access public
     */
    public static function check_path($path) {
        $exists = false;
        $isdir = false;
        $sftp = static::get_sftp_client();
        if (!$sftp) {
            return [false, false, false];
        } else {
            $exists = $sftp->file_exists($path);
            $isdir = $sftp->is_dir($path);
            $sftp->disconnect();
        }
        return [$exists, $isdir];
    }

    public static function upload_file($path, $localfile) {
        $sftp = static::get_sftp_client();
        if (!$sftp) {
            return $sftp;
        }
        $file = basename($localfile);
        $handle = @fopen($localfile, 'r');
        $target = "{$path}/{$file}";
        $result = $sftp->put($target, $handle, SFTP::SOURCE_LOCAL_FILE);
        $sftp->disconnect();
        return $result;
    }

    private static function get_sftp_client() {
        $config = config::get();
        if ($config->sftpauthtype == 'password') {
            $secret = $config->sftppass;
        } else {
            $secret = RSA::load($config->sftppass);
            // For old phpseclib version => $secret = new RSA(); $secret->loadKey($config->sftppass); .
        }
        $sftp = new SFTP($config->sftphost, $config->sftpport);
        if (!$sftp->login($config->sftpuser, $secret)) {
            $errors = array_merge(
                $sftp->getErrors(),
                $sftp->getSFTPErrors(),
                array(
                    $sftp->getStdError(),
                )
            );
            $phperror = error_get_last();
            if ($phperror) {
                $errors[] = $phperror['message'];
            }
            sftp_error::create_and_trigger($config->sftphost, $config->sftpport, $config->sftpauthtype, array_filter($errors));
            return $sftp = false;
        }
        return $sftp;
    }

    public static function get_all_selected_tables() {
        global $DB;
        $dbman = $DB->get_manager();
        $tables = array();
        $records = $DB->get_records('filesuploader_tables', array('selected' => 1), 'tablename ASC', 'tablename');
        // Because of that this function will be called when cron task is executing, Tables has to be verified if they
        // always exists, since it may be that tables were deleted (plugins were unistalled)
        // and till now no one has opened the table selection page (wich auto-update the table when called).
        foreach ($records as $value) {
            if ($dbman->table_exists($value->tablename)) {
                $tables[] = $value->tablename;
            }
        }
        return $tables;
    }

    public static function export_table($tablename, $exportconfig = array()) {
        global $DB;

        $tmpdir = make_temp_directory('filesuploader_tables_export', false);
        if (!check_dir_exists($tmpdir, true, true)) {
            return false;
        }
        $date = date("Ymd-Hi", time() - date("Z"));
        $ts = $date;
        $finalfile = $tablename.'_'.$ts.'.csv';
        $fp = fopen($tmpdir.'/'.$finalfile, 'w+');
        $delimiter = $exportconfig['delimiter'];
        if ($delimiter == "\t") {
            $delimiter = chr(9);
        }
        $tablerows = $DB->get_recordset($tablename);
        if ($tablerows->valid()) {
            $rowcount = 0;
            foreach ($tablerows as $row) {
                if ($rowcount == 0) {
                    $headers = get_object_vars ($row);
                    $rowcount++;
                    fputcsv($fp, array_keys($headers), $delimiter, '"');
                }
                $rowdata = get_object_vars ($row);
                fputcsv($fp, array_values($rowdata), $delimiter, '"');
            }
        }
        $tablerows->close();
        fclose($fp);
        $localpath = $tmpdir.'/'.$finalfile;
        try {
            static::set_encoding($localpath, $exportconfig['encoding']);
        } catch (\Exception $e) {
            return false;
        }
        try {
            $status = static::upload_file($exportconfig['path'], $localpath);
            if (!$status) {                
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
        
        return basename($localpath);
    }

    private static function set_encoding($localpath, $encoding) {
        $temppath = $localpath . 'temp';
        $source = fopen($localpath, 'r');
        $target = fopen($temppath, 'w');
        while (!feof($source)) {
            $line = mb_convert_encoding(fgets($source), $encoding);
            fwrite($target, $line);
        }
        fclose($source);
        fclose($target);
        unlink($localpath);
        rename($temppath, $localpath);
    }
}
