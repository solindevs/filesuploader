<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_filesuploader\task;

defined('MOODLE_INTERNAL') || die();

use lang_string;
use local_filesuploader\config;
use local_filesuploader\event\upload_files_error;
use local_filesuploader\sftp_helper;
use core_user;
use local_filesuploader\fake_user;
global $CFG;

require_once($CFG->dirroot . '/local/filesuploader/vendor/autoload.php');

class upload_files extends \core\task\scheduled_task
{
    
    public function get_name() {
        return new lang_string('uploadfiles', 'local_filesuploader');
    }
    
    public function execute() {
        $config = config::get();
        try {
            $executed = $this->do_execute();
            if ($executed && $config->emaildebugenabled == 1) {
                // Success inform admins per mail.
                $this->send_success_mail();
            }
        } catch (\Exception $e) {
            if ($config->emailenabled == 1) {
                // Fail inform admins per mail.
                upload_files_error::create_and_trigger($e);
            }
            throw $e;
        }
    }
    
    private function do_execute() {
        $config = config::get();
        $delimiter = ',';
        if ($config->csvseparator == 'custom') {
            $delimter = $config->customseparator;
        } else {
            $delimter = $config->csvseparator;
        }
        if ($delimiter == '\t') {
            $delimiter = chr(9);
        }
        if ($config->retryenabled == 1) {
            mtrace('  Retry count: ' . $this->get_retry_count());
            if ($this->get_retry_count() >= $config->retrytime) {
                mtrace('  Retries exhausted, giving up...');
                return false;
            }
        }
        if (empty($config->sftphost)) {
            mtrace(' Plugin not configured, skipping...');
            return false;
        }
        if (empty($config->filespath)) {
            mtrace(' Target path not configured, skipping...');
            return false;
        } else {
            $checks = sftp_helper::check_path($config->filespath);
            // Path not exists.
            if (!$checks[0]) {
                mtrace(' Target path not exists, skipping...');
                return false;
            }
            // Path is not directory.
            if (!$checks[1]) {
                mtrace(' Target path is not a directory, skipping...');
                return false;
            }
        }
        if ($config->archiveenabled == 1) {
            $archivepath = $config->archivepath;
            mtrace('        Archive path specified: ' . $archivepath);
            $checks = sftp_helper::check_path($config->archivepath);
            // Path not exists.
            if (!$checks[0]) {
                mtrace('        Target archive path not exists, skipping...');
                return false;
            }
            // Path is not directory.
            if (!$checks[1]) {
                mtrace('        Target archive path is not a directory, skipping...');
                return false;
            }           
            $status = sftp_helper::iterate_files($config->filespath, $archivepath);
            if (!$status) {
                mtrace('        Error when trying to connect to the sftp server.');
                return false;
            }
        } else {
            $archivepath = false;
            mtrace('        Archive path not specified. Old files will be deleted.');
            $status = sftp_helper::iterate_files($config->filespath, null);
            if (!$status) {
                mtrace('        Error when trying to connect to the sftp server.');
                return false;
            }
        }
        
        $exportconfig = array(
            'delimiter' => $delimter,
            'path' => $config->filespath,
            'encoding' => $config->csvencoding
        );
        
        $tables = sftp_helper::get_all_selected_tables();
        if (empty($tables)) {
            mtrace(" No tables are selected for upload.");
            return false;
        } else {
            $files = array();
            foreach ($tables as $table) {
                $filebasename = sftp_helper::export_table($table, $exportconfig);
                if ($filebasename) {
                    $files[] = $filebasename;
                } else {
                    mtrace("\t{$table} table could not be uploaded.");
                }
            }
            $count = count($files);
            mtrace("\t{$count} files uploaded.\n\tPath: {$config->filespath}.");
            foreach ($files as $file) {
                mtrace("\t\t+ File with name {$file} was successfully uploaded.");
            }
            return true;
        }
    }
    
    public function get_fail_delay() {
        $faildelay = parent::get_fail_delay();
        if (! $faildelay) {
            $config = config::get();
            $faildelay = $config->retryinterval;
        }
        if ($faildelay) {
            // Gotta divide it by two because Moodle insists in multiplying it.
            // The + 1 is to track how many times the task has failed.
            return ($faildelay + 1) / 2;
        } else {
            return null;
        }
    }
    
    private function get_retry_count() {
        $config = config::get();
        $faildelay = parent::get_fail_delay();
        return max(0, $faildelay - $config->retryinterval);
    }
    
    public function send_success_mail() {
        global $SITE;
        $config = config::get();
        if (empty($config->emailaddress)) {
            return;
        }
        $emails = explode(',', $config->emailaddress);
        foreach ($emails as $email) {
            $user = fake_user::get_external_user(trim($email));
            $noreply = core_user::get_noreply_user();
            $message = 'Cron task "Upload files" succesfully executed. at: '.date('Y.m.d-H:i');
            $subject = get_string('successmailsubject', 'local_filesuploader', $SITE->fullname);
            email_to_user($user, $noreply, $subject, strip_tags($message), $message);
        }
    }
}
