<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_filesuploader;

defined('MOODLE_INTERNAL') || die();

use admin_setting_configtextarea;

class admin_setting_configencryptedpassword extends admin_setting_configtextarea
{
    public function config_read($name) {
        $key = $this->encryption_key();
        $value = parent::config_read($name);
        $c = base64_decode($value);
        $ivlen = openssl_cipher_iv_length($cipher = 'aes-256-ctr');
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len = 32);
        $ciphertextraw = substr($c, $ivlen + $sha2len);
        $originalplaintext = @openssl_decrypt($ciphertextraw, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertextraw, $key, $asbinary = true);

        // PHP 5.6+ timing attack safe comparison.
        if (@hash_equals($hmac, $calcmac)) {
            return $originalplaintext;
        }

        return null;
    }

    public function config_write($name, $value) {
        $key = $this->encryption_key();
        $ivlen = openssl_cipher_iv_length($cipher = 'aes-256-ctr');
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertextraw = openssl_encrypt($value, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertextraw, $key, $asbinary = true);
        $value = base64_encode($iv . $hmac . $ciphertextraw);

        return parent::config_write($name, $value);
    }

    private function encryption_key() {
        $name = "{$this->name}_enckey";
        $enckey = get_config($this->plugin, $name);

        if (empty($enckey)) {
            $enckey = base64_encode(openssl_random_pseudo_bytes(16));
            set_config($name, $enckey, $this->plugin);
        }

        return base64_decode($enckey);
    }
}
