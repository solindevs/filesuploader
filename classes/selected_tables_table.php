<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_filesuploader;

class selected_tables_table extends \table_sql
{

    /**
     * Constructor
     *
     * @param int $uniqueid
     *            all tables have to have a unique id, this is used
     *            as a key when storing table properties like sort order in the session.
     */

    public function __construct($uniqueid) {
        parent::__construct($uniqueid);
        // Define the list of columns to show.
        $columns = array(
            'id',
            'tablename',
            'selected',
            'selectbox'
        );
        $this->define_columns($columns);

        // Define the titles of columns to show in header.
        $headers = array(
            'id',
            get_string('tablename', 'local_filesuploader'),
            get_string('selectedstatus', 'local_filesuploader'),
            'Select'
        );
        $this->define_headers($headers);
    }

    public function col_selectbox($values) {
        // If the data is being downloaded than we don't want to show HTML.
        if ($this->is_downloading()) {
            return null;
        } else {
            if ($values->selected == 1) {
                return ' <input type="checkbox" id="selecttable_'.
                $values->id.
                '" name="'.
                $values->id.
                '" value="selecttable_'.
                $values->id.
                '" checked>';
            } else {
                return ' <input type="checkbox" id="selecttable_'.
                $values->id.
                '" name="'.
                $values->id.
                '" value="selecttable_'.
                $values->id.
                '">';
            }
        }
    }

    public function col_selected($values) {
        if ($this->is_downloading()) {
            if (isset($values->selected)) {
                return $values->selected;
            } else {
                return null;
            }
        } else {
            global $OUTPUT;
            if (isset($values->selected) && $values->selected == 1) {
                return \html_writer::nonempty_tag('div',
                    $OUTPUT->pix_icon('check-circle-regular',
                        'alt',
                        'local_filesuploader',
                        array()), null);
            } else {
                return \html_writer::nonempty_tag('div',
                    $OUTPUT->pix_icon('times-circle-regular',
                        'alt',
                        'local_filesuploader',
                        array()), null);
            }
        }
    }

    public function col_tablename($values) {
        if ($this->is_downloading()) {
            if (isset($values->tablename)) {
                return $values->tablename;
            } else {
                return null;
            }
        } else {
            if (isset($values->tablename)) {
                return $values->tablename;
            } else {
                return "-";
            }
        }
    }

    public function col_id($values) {
        if ($this->is_downloading()) {
            if (isset($values->id)) {
                return $values->id;
            }
        } else {
            if (isset($values->id)) {
                return $values->id;
            }
        }
    }

    /**
     * This function is called for each data row to allow processing of
     * columns which do not have a *_cols function.
     *
     * @return string return processed value. Return NULL if no change has
     *         been made.
     */
    public function other_cols($colname, $value) {
    }
}