<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_filesuploader;
defined('MOODLE_INTERNAL') || die();

class fake_user extends \core_user {
    const EXTERNAL_USER = -45;
    public static $externaluser = false;

    public static function get_external_user($emailaddress) {
        global $CFG;
        if (empty(self::$externaluser)) {
            self::$externaluser = parent::get_dummy_user_record();
            self::$externaluser->id = self::EXTERNAL_USER;
            self::$externaluser->username = "external";
            self::$externaluser->lang = $CFG->lang;
            self::$externaluser->maildisplay = 1;
            self::$externaluser->emailstop = 0;
            self::$externaluser->mailformat = 1;
        }
        self::$externaluser->email = $emailaddress;
        self::$externaluser->firstname = $emailaddress;
        return self::$externaluser;
    }

    public static function get_user($userid, $fields = '*', $strictness = IGNORE_MISSING) {

        switch ($userid) {
            case self::EXTERNAL_USER:
                return self::get_external_user($strictness);
                break;
            default:
                return parent::get_user($userid, $fields, $strictness);
                break;
        }
    }
}
