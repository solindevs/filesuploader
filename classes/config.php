<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_filesuploader;

defined('MOODLE_INTERNAL') || die();

use dml_exception;

require_once($CFG->dirroot . '/lib/adminlib.php');
require_once($CFG->dirroot . '/local/filesuploader/classes/admin_setting_configencryptedpassword.php');


class config
{
    /**
     * Settings constructor.
     *
     * @param $settings
     */
    private function __construct($settings) {
        foreach (get_object_vars($settings) as $key => $value) {
            if ($key == 'sftppass') {
                $setting = new admin_setting_configencryptedpassword('local_filesuploader/sftppass', null, null, null);
                $value = $setting->get_setting();
            }
            $this->{$key} = $value;
        }
    }

    /**
     * @return config
     * @throws dml_exception
     */
    public static function get() {
        static $instance;

        if (is_null($instance)) {
            $config = get_config('local_filesuploader');
            $instance = new static($config);
        }

        return $instance;
    }

    public function __get($name) {
        if (isset($this->{$name})) {
            return $this->name;
        }
    }
}
