<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_filesuploader;

defined('MOODLE_INTERNAL') || die();

use core_user;

class error_mailer
{
    public static function intercept_error(\core\event\base $event) {
        global $SITE;

        $config = config::get();
        if (empty($config->emailaddress)) {
            return;
        }
        $emails = explode(',', $config->emailaddress);
        foreach ($emails as $email) {
            $user = fake_user::get_external_user(trim($email));
            $noreply = core_user::get_noreply_user();
            $message = $event->get_description();
            $subject = get_string('errormailsubject', 'local_filesuploader', $SITE->fullname);
            email_to_user($user, $noreply, $subject, strip_tags($message), $message);
        }
    }
}
