<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Files uploader plugin
 *
 * @package     local_filesuploader
 * @author      2021 Ahmed
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


namespace local_filesuploader\event;

defined('MOODLE_INTERNAL') || die();

use core\event\base;

class sftp_error extends base {
    /**
     * Override in subclass.
     *
     * Set all required data properties:
     *  1/ crud - letter [crud]
     *  2/ edulevel - using a constant self::LEVEL_*.
     *  3/ objecttable - name of database table if objectid specified
     *
     * Optionally it can set:
     * a/ fixed system context
     *
     * @return void
     */
    protected function init() {
        $this->context = \context_system::instance();

        $this->data['crud'] = 'r';
        $this->data['edulevel'] = self::LEVEL_OTHER;
    }

    /**
     * @param $host
     * @param $port
     * @param $authtype
     * @param $error
     * @return base
     * @throws \coding_exception
     */
    public static function create_and_trigger($host, $port, $authtype, $errors) {
        $event = static::create(array(
            'other' => compact('host', 'port', 'authtype', 'errors')
        ));
        $event->trigger();

        return $event;
    }

    public function get_description() {
        $data = $this->other;

        $host = $data['host'];
        $port = $data['port'];
        $authtype = $data['authtype'];
        $errors = $data['errors'];
        $errorsmsg = implode(PHP_EOL, $errors);

        if (empty($errorsmsg)) {
            $errorsmsg = 'Could not connect - unknown error. See scheduled task trace for additional debugging information.';
        }

        $description = "An error occurred during the SFTP transaction.

        <b>Error:</b>
        {$errorsmsg}
        <b>Host:</b> {$host}
        <b>Port:</b> {$port}
        <b>Auth type:</b> {$authtype}";

        return str_replace("\n", "\n<br>", $description);
    }
}
